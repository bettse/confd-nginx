FROM ubuntu:14.04.1
MAINTAINER Eric Betts <bettse@fastmail.fm>

# Install Nginx
RUN apt-get update
RUN apt-get install -y nginx-full curl

# Install confd
ADD confd-linux /usr/local/bin/confd

# Create directories
RUN mkdir -p /etc/confd/conf.d /etc/confd/templates

# Add confd files
ADD ./templates /etc/confd/templates/
ADD ./conf.d/ /etc/confd/conf.d/

# Remove default site
RUN rm -f /etc/nginx/sites-enabled/default
ADD nginx.conf /etc/nginx/nginx.conf

# Add boot script
ADD ./boot.sh /opt/boot.sh
RUN chmod +x /opt/boot.sh

EXPOSE 80

# Run the boot script
CMD /opt/boot.sh
